#!/bin/bash
######################
# utility to get info 
# from a server
# @antlas
######################


####################
# Just some colors #
####################
black="\033[0;30m"
gray="\033[1;30m"
red="\033[0;31m"
rose="\033[1;31m"
dgreen="\033[0;32m"
cgreen="\033[1;32m"
orange="\033[0;33m"
yellow="\033[1;33m"
dblue="\033[0;34m"
cblue="\033[1;34m"
dviolet="\033[0;35m"
cviolet="\033[1;35m"
dcyan="\033[0;36m"
ccyan="\033[1;36m"
cgray="\033[0;37m"
white="\033[1;37m"
neutral="\033[0;m"

##########
# DEFINES
##########
HOSTS_ALLOW="/etc/hosts.allow"
HOSTS_DENY="/etc/hosts.deny"
FAIL2BAN_LOG_FILE="/var/log/fail2ban.log"

function wait4key()
{
    read pause
}

[[ "$(id -u)" != "0" ]]\
	&& echo -e "${red}You need to be root...${neutral}"\
	&& exit 1

echo -e "######################"
echo -e "# Machine information"
echo -e "######################"
echo ""

echo -e "${orange}You are:${neutral}"
whoami
id

echo -e "${orange}System uptime:${neutral}"
uptime

echo -e "${orange}Kernel version:${neutral}"
uname -r

echo -e "${orange}RAM usage:${neutral}" 
free -h

echo -e "${orange}ROM usage:${neutral}" 
df -h

wait4key

# HOSTS DENY & ALLOW
echo -e "${orange}HOST deny:${neutral}" 
[[ -f "$HOSTS_DENY" ]]\
	&& cat "$HOSTS_DENY"
echo -e "${orange}HOST allow:${neutral}" 
[[ -f "$HOSTS_ALLOW" ]]\
	&& cat "$HOSTS_ALLOW"


wait4key

# fail2ban server
[[ -n "`which fail2ban`" ]]\
	&& echo -e "${orange}Fail2ban status: ${neutral}"\
	&& service fail2ban status\
	&& cat "$FAIL2BAN_LOG_FILE"

#fail2ban client
[[ -n "`which fail2ban-client`" ]]\
	&& echo -e "${orange}Fail2ban status ssh:${neutral}"\
	&& fail2ban-client status ssh

#denyhosts
[[ -n `which denyhosts` ]]\
	&& [[ -n $(pidof denyhosts) ]]\
	&& echo -e "${orange}Denyhosts status:${neutral}"\
	&& ps aux | grep denyhosts

echo -e "${orange}Listening ports:${neutral}" 
ss -tulpn

echo -e "${orange}Programs in listening:${neutral}" 
lsof -n | grep LISTEN

echo -e "${orange}Programs connected:${neutral}" 
lsof -n | grep ESTABLISHED

echo -e "${orange}Interfaces status:${neutral}" 
ip a

echo -e "${orange}Routage table:${neutral}" 
ip route

wait4key

echo -e "${orange}iptables status:${neutral}"
iptables -L

wait4key

[[ -n `which sensors` ]]\
	&& echo -e "${orange}Temperature status:${neutral}"\
	&& sensors
