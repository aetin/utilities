#!/usr/bin/env python

class Singleton(object):
	__instance = None

	def __new__(cls):
		if Singleton.__instance is None:
			Singleton.__instance = super(Singleton, cls).__new__(cls)
			Singleton.__instance.__initialized = False
		return Singleton.__instance

	def __init__(self):
		if (self.__initialized):
			return
		self.__initialized = True
		pass
