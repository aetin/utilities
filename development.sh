#!/bin/bash


########
# utils 
########
# find all logs changed in the last 10min
find /var/log -type f -cmin -10

# start new instance of firefox
firefox --new-instance --profile $(mktemp -d)

# clean firefox stuff
function ffclean()
{
  ll=`ps ux | grep firefox| wc -l`
  [[ $ll -gt 1 ]] \
    && killall firefox && bleachbit --clean firefox.*

  [[ $ll -eq 1 ]] \
    &&  bleachbit --clean firefox.*

  return 0
}

## strace 
strace process 2>&1 | grep word
strace -e open process 2>&1 | grep file
strace -e open,access 2>&1 | grep file
strace -p pid
strace -c -p pid
strace -e poll,select,connect,recvfrom,sendto nc www.example.com 80

## dirs
pushd .
popd
dirs -v
cd ~1

## avahi
avahi-browse -alr

## space usage
du -ah --max-depth=1

## prompt_command
function __prompt_command() {
    local EXIT=$?
    local RCol='\[\e[0m\]'
    local BRed='\[\e[1;41m\]'
    local BGre='\[\e[1;42m\]'
    local BYel='\[\e[1;43m\]'
    local BBlu='\[\e[1;44m\]'
    local BPur='\[\e[1;45m\]'
    PS1="${BGre}\u${RCol}${RCol}@${BBlu}\h:${BPur}\W${BYel}"
    local CodeCol=${BGre}
	  [[ $EXIT -ne 0 ]]\
		    && CodeCol=${BRed}
	  PS1+="${CodeCol}[?=$EXIT]\$${RCol} "
}
export PROMPT_COMMAND=__prompt_command

## history
source !$
!42 # exe 42th command in $(history)

function enfr()
{
  [[ -z "$1" ]]\
    && echo "enfr <word>" && return 1
  w3m "http://www.wordreference.com/enfr/$1"
}

function fren()
{
  [[ -z "$1" ]]\
    && echo "fren <mot>" && return 1
  w3m "http://www.wordreference.com/fren/$1"
}

# find specific file
function fff()
{
  [[ -z "$1" ]] && echo "usage : $fff <expr>" && return 1

  find . -iname "*$1*" -print
}

########
# email
########
echo "This is a message" | /usr/bin/mailx -v -r "sender@example.com" \
    -s "Subject" \
    -S smtp=smtps://smtp.isp_example.com:465 \
    -S smtp-use-starttls \
    -S smtp-auth=login \
    -S smtp-auth-user="$user" \
    -S smtp-auth-password="$password" \
    -S ssl-verify=ignore \
    "recipient@example.org"

############
# svn utils
############
# alias for remote svn diff
function sdiff()
{
  [[ -z "$1" ]]\
    && echo "sdiff <rev>" && return 1

  svn diff -c "$1" https://10.0.0.1/svn
}

# current diff on local tree
function currentDiff()
{
  #svn info "$PWD"
  #curr=svn info | grep "^Last Changed Rev:"| cut -d ":" -f2 | cut -c 2-
  curr=svn info | grep -i "^Last changed rev"| cut -d ":" -f2 | cut -c 2-
  prev=expr $curr - 1

  svn diff . -r "r$prev:r$curr"
}

# svn up
alias svu='svn up'
# svn status
alias stq='svn st -q'
# svn diff
alias std='svn diff'
# svn pe svn:externals
alias spe="svn pe svn:externals"

# svn log
function svl() {
        local ppath="."
        [[ -n "$1" ]] && [[ -e "$1" ]]\
                && ppath="$1"
        svn log -l 50 $ppath
}

# svn blame
function svb() {
        [[ -z "$1" ]] || [[ ! -e "$1" ]] || [[ ! -f "$1" ]]\
                && echo "usage: svb <path_to_file>" && return 1 
        local ppath="$1"     
        svn blame $ppath
}

################
# installation
################
alias aps="apt-cache search"
alias api="sudo apt install"
function dpl()
{
  [[ -z "$1" ]]\
    && echo "dpl <pattern>" && return 1
  local pattern="$1"
  
  dpkg -l "*$pattern*"
}

function dps()
{
  [[ -z "$1" ]]\
    && echo "dps <pattern>" && return 1
  local pattern="$1"
  
  dpkg -S "*$pattern*"
}

#############
# MISC
#############
# check sd card IO
apt install f3
# write test
f3write /media/$USER/sdcard
# read test
f3read !$


#############
# iptables
#############
# add rule to drop ips in ipset called badips_intruders
iptables -I INPUT -m set --match-set myset src -j DROP

# create ipset
ipset create myset hash:net

# add ip to ipset
ipset add myset X.X.X.X