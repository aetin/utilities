#!/bin/bash
#########################
# little tool to quickly
# download images
# and display them
# @antlas
#########################

# check for the display binary (eog)
# and download if it is found
function display_images_from_url()
{
  [[ -z `which eog` ]]\
	&& echo "[E] : eog not installed. exiting"\
	&& echo "(sudo apt install eog)"\
	&& return 1

  local tmpdir=$(mktemp -d)
  declare -a array=("${!1}")

  echo "Temporary directory is $tmpdir"
  cd "$tmpdir"
  echo "==============================="
  for i in "${array[@]}";do
    echo "Getting $i"
    wget "$i" 2>/dev/null
  done
  echo "==============================="

  eog *

  cd "$HOME"
  rm -rf "$tmpdir"
}

# create placeholder for
# images links
function aurora()
{

  local addrs=("http://services.swpc.noaa.gov/images/aurora-forecast-northern-hemisphere.jpg"\
		"http://services.swpc.noaa.gov/images/aurora-forecast-southern-hemisphere.jpg"\
		"http://aurorasnow.fmi.fi/public_service/images/latest_HOV.jpg"\
		"http://aurorasnow.fmi.fi/public_service/images/latest_DYN.jpg"\
		"http://services.swpc.noaa.gov/images/wing-kp-24-hour.gif"\
		"https://www.spaceweatherlive.com/images/grafieken/aurora-map2.jpg")

  display_images_from_url addrs[@]
}

# main
aurora